package com.isolutions.taxi5;

import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import com.isolutions.taxi5.Utils.DestinationListAdapter;
import com.isolutions.taxi5.Utils.Scaler;
import com.isolutions.taxi5.Utils.Utils;
import com.isolutions.taxi5api.Models.Location;
import com.isolutions.taxi5api.rest.TaxiDataProvider;

import java.util.ArrayList;

/**
 * Created by Dmitriy Zheltko on 09.01.2015.
 */
public class SearchPage extends Fragment {

    public static final String LOCATION = "location";
    View rootView;
    @InjectView(R.id.search) EditText searchView;
    @InjectView(R.id.list) ListView list;
    @InjectView(R.id.progressBar) ProgressBar loading;
    @InjectView(R.id.clear_search) View clearSearch;
    ArrayList<Location> availableAdresses;
    DestinationListAdapter adapter;
    AvailableAdressesGeter geter;
    Activity activity;
    ActionBar actionBar;
    TaxiDataProvider dataProvider;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.inject(this, rootView);
        activity = getActivity();
        actionBar = ((ActionBarActivity) activity).getSupportActionBar();
        actionBar.hide();
        dataProvider = new TaxiDataProvider(getActivity());
        availableAdresses = new ArrayList<Location>();
        adapter = new DestinationListAdapter(activity, R.layout.item_destination, availableAdresses);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                activity.getIntent().putExtra(LOCATION, availableAdresses.get(position));
                activity.onBackPressed();
            }
        });
        searchView.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    clearSearch.setVisibility(View.VISIBLE);
                } else {
                    clearSearch.setVisibility(View.GONE);
                }
                getAddress(s.toString());
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        Scaler.scaleContents(rootView);

        return rootView;
    }

    private void getAddress(String address) {
        if (geter != null) {
            geter.cancel(true);
        }
        geter = new AvailableAdressesGeter();
        geter.execute(address);
    }

    class AvailableAdressesGeter extends AsyncTask<String, Void, Boolean> {

        String searchElement;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading.setVisibility(View.VISIBLE);

        }

        @Override
        protected Boolean doInBackground(String... params) {
            searchElement = params[0];
            availableAdresses.removeAll(availableAdresses);
            try {
                availableAdresses.addAll(dataProvider.getLocations(searchElement));
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (result) {
                adapter.setSearchElement(searchElement);
                adapter.notifyDataSetChanged();
                list.invalidate();
            } else {
                Utils.toast(getActivity(), "Отсутствует соединение с интернетом");
            }
            loading.setVisibility(View.GONE);

        }

    }

    @OnClick(R.id.clear_search)
    public void clearSearch() {
        searchView.setText("");
    }

    @Override
    public void onPause() {
        super.onPause();
        Utils.hideKeyboard(rootView, getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            getAddress("");
        } catch (Exception e) {
            Utils.toast(getActivity(), "Отсутствует соединение с интернетом");
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        actionBar.show();
    }
}

