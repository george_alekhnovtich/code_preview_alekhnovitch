package com.isolutions.taxi5;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.isolutions.taxi5.Utils.Scaler;
import com.isolutions.taxi5.Utils.Utils;
import com.isolutions.taxi5api.Models.*;

import java.util.ArrayList;

/**
 * Created by Dmitriy Zheltko on 08.01.2015.
 */
public class MainPage extends Fragment {

    public static final String USER_NAME = "user_name";
    private static final String PHONE_NUMBER = "phone_number";
    View rootView;
    Location location;
    SharedPreferences sharedPreferences;
    @InjectView(R.id.name) EditText nameView;
    @InjectView(R.id.phone) EditText phoneView;
    @InjectView(R.id.street) EditText streetView;
    @InjectView(R.id.house) EditText houseView;
    @InjectView(R.id.housing) EditText housingView;
    @InjectView(R.id.porch) EditText porchView;
    @InjectView(R.id.comment) EditText commentView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_main, container, false);
        if (rootView.getLayoutParams() == null) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            rootView.setLayoutParams(params);
        }
        ButterKnife.inject(this, rootView);
        Scaler.scaleContents(rootView);
        sharedPreferences = getActivity().getSharedPreferences(MainActivity.TAXI_5_MAIN, Context.MODE_PRIVATE);
        nameView.setText(sharedPreferences.getString(USER_NAME, ""));
        phoneView.setText(sharedPreferences.getString(PHONE_NUMBER, ""));
        nameView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                sharedPreferences.edit().putString(USER_NAME, s.toString()).commit();
            }
        });
        phoneView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus && phoneView.getText().toString().equals("")) {
                    phoneView.setText("+375");
                    phoneView.invalidate();
                }
            }
        });
        phoneView.addTextChangedListener(new TextWatcher() {

            boolean shouldReplace;
            boolean isBracket;

            public void afterTextChanged(Editable s) {
                if (shouldReplace && !isBracket) {
                    isBracket = true;
                    switch (s.length()) {
                        case 4:
                            phoneView.append("(");
                            break;
                        case 5:
                            phoneView.setText(s.insert(4, "("));
                            phoneView.setSelection(6);
                            break;
                        case 7:
                            phoneView.append(")");
                            break;
                        case 8:
                            phoneView.setText(s.insert(7, ")"));
                            phoneView.setSelection(9);
                            break;
                        case 11:
                            phoneView.append("-");
                            break;
                        case 12:
                            phoneView.setText(s.insert(11, "-"));
                            phoneView.setSelection(13);
                            break;
                        case 14:
                            phoneView.append("-");
                            break;
                        case 15:
                            phoneView.setText(s.insert(14, "-"));
                            phoneView.setSelection(16);
                            break;


                    }
                }
                isBracket = false;

                sharedPreferences.edit().putString(PHONE_NUMBER, s.toString()).commit();
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (start == 0 && after == 0) {
                    phoneView.setText("+");
                    phoneView.setSelection(1);
                }

                shouldReplace = !(count == 1 && after == 0);
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        houseView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                validation();
            }
        });
        return rootView;
    }

    @OnClick(R.id.call_button)
    public void call() {
        Utils.call(getActivity());
    }

    @OnClick(R.id.order_button)
    public void order() {
        String name = nameView.getText().toString();
        String phone = phoneView.getText().toString();
        String house = houseView.getText().toString();
        String housing = housingView.getText().toString();
        String porch = porchView.getText().toString();
        String comment = "Подъезд " + porch +"; " + commentView.getText().toString();
        if (location == null) {
            location = new Location();
        }
        location.house = house;
        location.section = housing;
        location.porch = porch;
        location.comment = comment;
        Client client = new Client();
        client.name = name;
        client.phone = phone;
        Route route = new Route();
        route.name = name;
        route.type = "address";
        route.location = location;
        route.comment = comment;
        ArrayList<Route> routes = new ArrayList<Route>();
        routes.add(route);
        Order order = new Order();
        order.client = client;
        order.route = routes;
        //order.comment = comment;

        //К.О. тестовый режим
        if (comment.equals("TESTMODE")) {
            Utils.toast(getActivity(), "Тестовый режим.");
            Options options = new Options();
            options.testOrders = 1;
            order.options = options;
        }

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER_NAME, name);
        editor.putString(PHONE_NUMBER, phone);
        editor.commit();


        if (validation()) {
            Intent intent = new Intent(getActivity(), StatusActivity.class);
            intent.putExtra(StatusActivity.ORDER, order);
            startActivity(intent);
        }

    }

    private boolean validation() {
        boolean isValid = true;
        try {
            PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
            Phonenumber.PhoneNumber number = phoneNumberUtil.parse(phoneView.getText().toString(), "BLR");
            Utils.log(number.toString());
            isValid = phoneNumberUtil.isValidNumber(number);
            if (!isValid) {
                phoneView.setError("Неправильный номер телефона");
            }
        } catch (NumberParseException e) {
            e.printStackTrace();
        }

        String house = houseView.getText().toString();
        //if (!house.matches("^([0-9]+[^\\s]*)?$") || house.equals("")) {
        if (house.equals("")) {
            houseView.setError("Неправильный номер дома");
            isValid = false;
        } else {
            houseView.setError(null);
        }
        String porch = porchView.getText().toString();
        if (!porch.matches("^([0-9]+[^\\s]*)?$")) {
            porchView.setError("Неправильный номер подъезда");
            isValid = false;
        } else {
            porchView.setError(null);
        }
        if (streetView.getText().toString().equals("")) {
            streetView.setError("Нужно выбрать улицу");
            isValid = false;
        } else {
            streetView.setError(null);
        }
        return isValid;
    }

    @OnClick(R.id.street)
    public void findStreet() {
        FragmentManager fragmentManager = getFragmentManager();
        Fragment fragment = new SearchPage();
        fragmentManager.beginTransaction().replace(R.id.container, fragment).addToBackStack(null).commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        location = (Location) getActivity().getIntent().getSerializableExtra(SearchPage.LOCATION);
        if (location != null) {
            streetView.setText(location.street);
            houseView.setText(location.house);
            housingView.setText(location.section);
            if (location.name != null) {
                commentView.setText("Заведение: " + location.name);
            } else {
                if (commentView.getText().toString().startsWith("Заведение:")) {
                    commentView.setText("");
                }
            }
            validation();
        }
        ((MainActivity) getActivity()).setInfoVisible(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        Utils.hideKeyboard(rootView, getActivity());
    }
}
