package com.isolutions.taxi5.Widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.HorizontalScrollView;

/**
 * Created by George Alekhovitch on 13.01.2015.
 */
//can render all views even if layout goes out of container width
public class OverviewLayout extends HorizontalScrollView {
    public OverviewLayout(Context context) {
        super(context);
    }

    public OverviewLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public OverviewLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return true;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return false;
    }
}
