package com.isolutions.taxi5;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Window;
import android.view.WindowManager;
import com.isolutions.taxi5.Utils.Scaler;
import com.isolutions.taxi5api.Models.Order;
import com.isolutions.taxi5api.rest.TaxiDataProvider;

import java.util.concurrent.TimeUnit;

/**
 * Created by George Alekhovitch on 14.01.2015.
 */
public class PreloaderActivity extends Activity {
    private CountDownTimer lTimer;
    private TaxiDataProvider dataProvider;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        setContentView(R.layout.activity_preloader);

        Scaler.init(Scaler.getWidth(this), Scaler.getHeight(this));
        dataProvider = new TaxiDataProvider(this);
        new Thread(new Runnable() {
            @Override
            public void run() {

                Order order = dataProvider.getOrder();
                if (order != null) {
                    closeScreen(true, order);
                } else {
                    closeScreen(false, order);
                }
            }
        }).start();
    }

    private void closeScreen(Boolean haveStatus, Order order) {
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        startActivity(intent);
        if (haveStatus) {
            intent = new Intent();
            intent.setClass(this, StatusActivity.class);
            intent.putExtra(StatusActivity.ORDER, order);
            startActivity(intent);
        }

        finish();
    }
}
