package com.isolutions.taxi5;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import com.isolutions.taxi5.Utils.Scaler;
import com.isolutions.taxi5.Utils.Utils;
import com.isolutions.taxi5api.Models.Car;
import com.isolutions.taxi5api.Models.Order;
import com.isolutions.taxi5api.rest.State;
import com.isolutions.taxi5api.rest.StateChangeListener;
import com.isolutions.taxi5api.rest.TaxiDataProvider;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import java.util.Calendar;
import java.util.Map;

/**
 * Created by George Alekhovitch on 10.01.2015.
 */
public class StatusActivity extends Activity implements StateChangeListener {

    @InjectView(R.id.state_layout) View stateLayout;
    @InjectView(R.id.os_layout) View orderStateLayout;
    @InjectView(R.id.nic_layout) View noInternetConnectionLayout;
    @InjectView(R.id.oc_layout) View orderCanceledLayout;
    @InjectView(R.id.cnf_layout) View carNotFoundLayout;
    @InjectView(R.id.sc_layout) View searchCarLayout;
    @InjectView(R.id.driver) TextView driver;
    @InjectView(R.id.number) TextView number;
    @InjectView(R.id.text_waiting_time) TextView waitingTimeText;
    @InjectView(R.id.waiting_time) TextView waitingTime;
    @InjectView(R.id.state) TextView textStatusTop;
    @InjectView(R.id.text_status) TextView textStatusBottom;
    @InjectView(R.id.accept_order_button) Button acceptOrder;
    @InjectView(R.id.accept_text) TextView acceptText;
    @InjectView(R.id.car) ImageView car;
    @InjectView(R.id.bricks) View bricks;
    @InjectView(R.id.nic_call_button) ImageView call_button;
    @InjectView(R.id.main_layout) View mainLayout;
    @InjectView(R.id.check) TextView checkView;
    @InjectView(R.id.text_check) TextView checkViewText;
    @InjectView(R.id.close) TextView closeView;
    public static final String ORDER = "order";
    TaxiDataProvider dataProvider;
    Animation carDrive;
    Animation carStop;
    Animation bricksAnim;
    boolean isCarAnimated;
    boolean isOrderAcceptable;
    boolean isDelivered;
    boolean isOrderCompleted;
    boolean isCountDownStarted;
    CountDownTimer countDownTimer;
    CountDownTimer countDownEta;
    String deliverTime;
    State state;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.gc();
        setContentView(R.layout.activity_status);
        ButterKnife.inject(this);
        Scaler.scaleContents(findViewById(R.id.main));
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);


        carStop = AnimationUtils.loadAnimation(this,
                R.anim.car_stop);
        bricksAnim = AnimationUtils.loadAnimation(this,
                R.anim.bricks_anim);
        carDrive = AnimationUtils.loadAnimation(this,
                R.anim.car_drive);
        carStop.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                car.setImageResource(R.drawable.passat_stay);
                isCarAnimated = false;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        tryFindCar();
    }

    @OnClick({R.id.cnf_call_button, R.id.nic_call_button, R.id.oc_call_button})
    public void call() {
        Utils.call(this);
    }


    @OnClick(R.id.close)
    public void close() {
        if (state != null && (state.equals(State.STATUS_CAR_SEARCH) || state.equals(State.STATUS_CAR_FOUND))) {
            dataProvider.cancel();
        }
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @OnClick(R.id.accept_order_button)
    public void acceptOrder() {
        if (isOrderAcceptable) {
            dataProvider.approveOrder();
        } else {
            dataProvider.cancelOrderRequests();
            tryFindCar();
        }
    }

    @OnClick(R.id.cnf_try_again)
    public void tryFindCar() {
        dataProvider = new TaxiDataProvider(this);
        isCountDownStarted = false;
        dataProvider.setStateChangeListener(this);
        Order order = (Order) getIntent().getSerializableExtra(ORDER);
        if (order.id == null) {
            dataProvider.sendOrder(order);
        } else {
            dataProvider.resetConnection(order);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dataProvider.cancelOrderRequests();
    }

    @Override
    public void OnStateChange(Order order) {
        state = State.fromString(order.status);
        switch (state) {
            case STATUS_CAR_SEARCH:
                hideAllLayouts();
                searchCarLayout.setVisibility(View.VISIBLE);
                mainLayout.setBackgroundResource(R.color.orange_back);
                break;
            case STATUS_CAR_FOUND:
                hideAllLayouts();
                stateLayout.setVisibility(View.VISIBLE);
                orderStateLayout.setVisibility(View.VISIBLE);
                acceptOrder.setVisibility(View.VISIBLE);
                acceptText.setVisibility(View.VISIBLE);
                countDownTimer = new CountDownTimer(60000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        acceptText.setText("Осталось " + millisUntilFinished / 1000 + " сек");
                    }

                    public void onFinish() {
                        acceptText.setText("Время подтверждения закончилось");
                        acceptOrder.setText("Повторить заказ");
                        isOrderAcceptable = false;
                    }
                };
                countDownTimer.start();
                isOrderAcceptable = true;
                acceptOrder.setText("Подтвердить заказ");
                textStatusTop.setText("Автомобиль найден");
                setDriverName(order);
                mainLayout.setBackgroundResource(R.color.purple_back);
                break;
            case STATUS_CAR_APPROVED:
                hideNotStateLayouts();
                stateLayout.setVisibility(View.VISIBLE);
                orderStateLayout.setVisibility(View.VISIBLE);
                textStatusTop.setText("Автомобиль спешит к вам");
                textStatusBottom.setVisibility(View.VISIBLE);
                textStatusBottom.setText("Спасибо, что выбрали нас");
                setDriverName(order);
                animateCar();
                mainLayout.setBackgroundResource(R.color.purple_back);
                closeView.setText("Закрыть");
                break;
            case STATUS_CLIENT_APPROVE_TIMEOUT:
                hideNotStateLayouts();
                acceptText.setText("Время подтверждения закончилось");
                acceptOrder.setText("Повторить заказ");
                isOrderAcceptable = false;
                cancelCountDowns();
                setDriverName(order);
                mainLayout.setBackgroundResource(R.color.purple_back);
                closeView.setText("Закрыть");
                break;
            case STATUS_CAR_DELIVERING:
                hideNotStateLayouts();
                stateLayout.setVisibility(View.VISIBLE);
                orderStateLayout.setVisibility(View.VISIBLE);
                textStatusTop.setText("Автомобиль спешит к вам");
                textStatusBottom.setVisibility(View.VISIBLE);
                textStatusBottom.setText("Спасибо, что выбрали нас");
                setDriverName(order);
                mainLayout.setBackgroundResource(R.color.purple_back);
                animateCar();
                closeView.setText("Закрыть");
                break;
            case STATUS_CAR_DELIVERED:
                hideNotStateLayouts();
                cancelCountDowns();
                stateLayout.setVisibility(View.VISIBLE);
                orderStateLayout.setVisibility(View.VISIBLE);
                bricks.clearAnimation();
                car.startAnimation(carStop);
                textStatusTop.setText("Автомобиль доставлен");
                textStatusBottom.setVisibility(View.VISIBLE);
                textStatusBottom.setText("Автомобиль ждёт вас");
                waitingTimeText.setText("ВРЕМЯ ПРИБЫТИЯ");
                if (!isDelivered) {
                    deliverTime = java.text.DateFormat.getTimeInstance().format(Calendar.getInstance().getTime());
                    isDelivered = true;
                }
                setDriverName(order);
                waitingTime.setText(deliverTime);
                mainLayout.setBackgroundResource(R.color.blue_back);
                closeView.setText("Закрыть");
                break;
            case STATUS_ORDER_POSTPONED:
                break;
            case STATUS_ORDER_IN_PROGRESS:
                hideNotStateLayouts();
                cancelCountDowns();
                stateLayout.setVisibility(View.VISIBLE);
                orderStateLayout.setVisibility(View.VISIBLE);
                textStatusTop.setText("Вы в пути");
                textStatusBottom.setVisibility(View.VISIBLE);
                textStatusBottom.setText("К вашим услугам\nбесплатный Wi-Fi");
                waitingTime.setTextColor(getResources().getColor(R.color.purple));
                waitingTime.setVisibility(View.INVISIBLE);
                waitingTimeText.setVisibility(View.INVISIBLE);
                setDriverName(order);
                animateCar();
                mainLayout.setBackgroundResource(R.color.blue_back);
                closeView.setText("Закрыть");
                break;
            case STATUS_ORDER_COMPLETED:
                hideNotStateLayouts();
                stateLayout.setVisibility(View.VISIBLE);
                orderStateLayout.setVisibility(View.VISIBLE);
                textStatusTop.setText("Вы на месте");
                textStatusBottom.setVisibility(View.VISIBLE);
                textStatusBottom.setText("Пожалуйста, оплатите поездку");
                waitingTime.setVisibility(View.VISIBLE);
                waitingTimeText.setVisibility(View.VISIBLE);
                waitingTimeText.setText("ВРЕМЯ ПРИБЫТИЯ");
                setDriverName(order);
                if (!isOrderCompleted) {
                    deliverTime = java.text.DateFormat.getTimeInstance().format(Calendar.getInstance().getTime());
                    isOrderCompleted = true;
                }
                waitingTime.setText(deliverTime);
                checkView.setVisibility(View.VISIBLE);
                checkViewText.setVisibility(View.VISIBLE);
                checkView.setText("Не оплачен");
                checkView.setTextColor(Color.RED);
                bricks.clearAnimation();
                car.startAnimation(carStop);
                mainLayout.setBackgroundResource(R.color.green_back);
                closeView.setText("Закрыть");
                break;
            case STATUS_ORDER_PAID:
                hideNotStateLayouts();
                stateLayout.setVisibility(View.VISIBLE);
                orderStateLayout.setVisibility(View.VISIBLE);
                textStatusTop.setText("Вы на месте");
                textStatusBottom.setVisibility(View.VISIBLE);
                textStatusBottom.setText("Спасибо что выбрали наш сервис");
                waitingTime.setVisibility(View.VISIBLE);
                waitingTimeText.setVisibility(View.VISIBLE);
                setDriverName(order);
                if (!isOrderCompleted) {
                    deliverTime = java.text.DateFormat.getTimeInstance().format(Calendar.getInstance().getTime());
                    isOrderCompleted = true;
                }
                waitingTime.setText(deliverTime);
                checkView.setVisibility(View.VISIBLE);
                checkViewText.setVisibility(View.VISIBLE);
                checkView.setText("Оплачен");
                checkView.setTextColor(getResources().getColor(R.color.blue));
                bricks.clearAnimation();
                mainLayout.setBackgroundResource(R.color.green_back);
                closeView.setText("Закрыть");
                break;
            case STATUS_ORDER_CLOSED:
                hideNotStateLayouts();
                stateLayout.setVisibility(View.VISIBLE);
                orderStateLayout.setVisibility(View.VISIBLE);
                textStatusTop.setText("Вы на месте");
                textStatusBottom.setVisibility(View.VISIBLE);
                textStatusBottom.setText("Спасибо что выбрали наш сервис");
                waitingTime.setVisibility(View.VISIBLE);
                waitingTimeText.setVisibility(View.VISIBLE);
                setDriverName(order);
                if (!isOrderCompleted) {
                    deliverTime = java.text.DateFormat.getTimeInstance().format(Calendar.getInstance().getTime());
                    isOrderCompleted = true;
                }
                waitingTime.setText(deliverTime);
                checkView.setVisibility(View.VISIBLE);
                checkViewText.setVisibility(View.VISIBLE);
                checkView.setText("Оплачен");
                checkView.setTextColor(getResources().getColor(R.color.blue));
                bricks.clearAnimation();
                mainLayout.setBackgroundResource(R.color.green_back);
                closeView.setText("Закрыть");
                break;
            case STATUS_ORDER_NOT_PAID:
                hideNotStateLayouts();
                stateLayout.setVisibility(View.VISIBLE);
                orderStateLayout.setVisibility(View.VISIBLE);
                textStatusTop.setText("Вы на месте");
                textStatusBottom.setVisibility(View.VISIBLE);
                textStatusBottom.setText("Пожалуйста, оплатите поездку");
                waitingTime.setVisibility(View.VISIBLE);
                waitingTimeText.setVisibility(View.VISIBLE);
                waitingTimeText.setText("ВРЕМЯ ПРИБЫТИЯ");
                setDriverName(order);
                if (!isOrderCompleted) {
                    deliverTime = java.text.DateFormat.getTimeInstance().format(Calendar.getInstance().getTime());
                    isOrderCompleted = true;
                }
                waitingTime.setText(deliverTime);
                checkView.setVisibility(View.VISIBLE);
                checkViewText.setVisibility(View.VISIBLE);
                checkView.setText("Не оплачен");
                checkView.setTextColor(Color.RED);
                bricks.clearAnimation();
                car.startAnimation(carStop);
                mainLayout.setBackgroundResource(R.color.green_back);
                closeView.setText("Закрыть");
                break;
            case STATUS_CANCELED:
                hideAllLayouts();
                stateLayout.setVisibility(View.VISIBLE);
                orderCanceledLayout.setVisibility(View.VISIBLE);
                mainLayout.setBackgroundResource(R.color.red_back);
                closeView.setText("Закрыть");
                break;
            case STATUS_FORCE_CANCELED:
                hideAllLayouts();
                stateLayout.setVisibility(View.VISIBLE);
                orderCanceledLayout.setVisibility(View.VISIBLE);
                mainLayout.setBackgroundResource(R.color.red_back);
                closeView.setText("Закрыть");
                break;
            case STATUS_NO_INTERNET_CONNECTION:
                hideAllLayouts();
                stateLayout.setVisibility(View.VISIBLE);
                noInternetConnectionLayout.setVisibility(View.VISIBLE);
                mainLayout.setBackgroundResource(R.color.red_back);
                closeView.setText("Закрыть");
                break;
            case STATUS_CAR_NOT_FOUNDED:
                hideAllLayouts();
                stateLayout.setVisibility(View.VISIBLE);
                carNotFoundLayout.setVisibility(View.VISIBLE);
                mainLayout.setBackgroundResource(R.color.red_back);
                closeView.setText("Закрыть");
                break;
            case STATUS_CLIENT_REJECT:
                hideAllLayouts();
                orderCanceledLayout.setVisibility(View.VISIBLE);
                mainLayout.setBackgroundResource(R.color.red_back);
                closeView.setText("Закрыть");
                break;
            default:
        }
    }

    private void setDriverName(Order order) {
        driver.setText(((Map<String, String>) order.car).get(Car.DRIVER_NAME));
        if (!isCountDownStarted) {
            isCountDownStarted = true;
            Utils.log("isCountDownStarted");
            if (countDownEta != null) {
                countDownEta.cancel();
            }
            countDownEta = new CountDownTimer(order.eta.getEtaMilliseconds(), 1000) {

                public void onTick(long millisUntilFinished) {

                    Utils.log("onTick");
                    waitingTime.setText(getTime(millisUntilFinished, false));
                }

                public void onFinish() {
                    Utils.log("onFinish");
                    countDownEta = new CountDownTimer(2147400000, 1000) {
                        private long time;

                        @Override
                        public void onTick(long millisUntilFinished) {
                            Utils.log("onTick");
                            time += 1000;
                            waitingTime.setTextColor(Color.RED);
                            waitingTime.setText(getTime(time, true));
                        }

                        @Override
                        public void onFinish() {

                        }
                    };
                    countDownEta.start();
                }
            };
            countDownEta.start();

        }


        number.setText(((Map<String, String>) order.car).get(Car.LISENSE));
    }

    private String getTime(long time, boolean isInvert) {
        int hours = (int) (time / 3600000);
        int minutes = (int) ((time - hours * 3600000) / 60000);
        int seconds = (int) ((time - hours * 3600000 - minutes * 60000) / 1000);
        Utils.log(String.valueOf(seconds));
        return (isInvert ? "-" : "" + (hours == 0 ? "" : (hours + " часов\n")))
                + (minutes != 0 ? (minutes + " минут\n") : "") + ((hours == 0) ? (seconds + " cекунд") : "");

    }

    public void cancelCountDowns(){
        if(countDownEta!=null){
            countDownEta.cancel();
        }
        if(countDownTimer!=null){
            countDownTimer.cancel();
        }
    }
    private void animateCar() {
        if (!isCarAnimated) {
            hideNotStateLayouts();
            stateLayout.setVisibility(View.VISIBLE);
            orderStateLayout.setVisibility(View.VISIBLE);
            acceptOrder.setVisibility(View.GONE);
            acceptText.setVisibility(View.GONE);
            orderStateLayout.setVisibility(View.VISIBLE);
            car.setImageResource(R.drawable.passatpic_run);
            car.startAnimation(carDrive);
            bricks.startAnimation(bricksAnim);
            isCarAnimated = true;
        }
    }

    public void hideAllLayouts() {
        stateLayout.setVisibility(View.GONE);
        orderStateLayout.setVisibility(View.GONE);
        noInternetConnectionLayout.setVisibility(View.GONE);
        orderCanceledLayout.setVisibility(View.GONE);
        carNotFoundLayout.setVisibility(View.GONE);
        searchCarLayout.setVisibility(View.GONE);
    }

    public void hideNotStateLayouts() {
        noInternetConnectionLayout.setVisibility(View.GONE);
        orderCanceledLayout.setVisibility(View.GONE);
        carNotFoundLayout.setVisibility(View.GONE);
        searchCarLayout.setVisibility(View.GONE);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(new CalligraphyContextWrapper(newBase));
    }
}
