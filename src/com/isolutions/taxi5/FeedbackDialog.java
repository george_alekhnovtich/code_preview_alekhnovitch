package com.isolutions.taxi5;

import android.app.DialogFragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.*;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import com.isolutions.taxi5.Utils.Scaler;
import com.isolutions.taxi5.Utils.Utils;
import com.isolutions.taxi5api.Models.Feedback;
import com.isolutions.taxi5api.rest.TaxiDataProvider;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by George Alekhovitch on 05.02.2015.
 */
public class FeedbackDialog extends DialogFragment {

    @InjectView(R.id.name) TextView name;
    @InjectView(R.id.phone) TextView phone;
    @InjectView(R.id.message) TextView message;
    View rootView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        rootView = inflater.inflate(R.layout.dialog_fragment_leave_feedback, container, false);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        rootView.setLayoutParams(params);
        Scaler.scaleContents(rootView);
        ButterKnife.inject(this, rootView);
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(MainActivity.TAXI_5_MAIN, Context.MODE_PRIVATE);

        name.setText(sharedPreferences.getString(MainPage.USER_NAME, ""));

        return rootView;
    }

    @OnClick(R.id.cancel)
    public void cancel() {
        hideDialog();
    }

    private void hideDialog() {
        Utils.hideKeyboard(rootView, getActivity());
        dismiss();
    }

    @OnClick(R.id.send)
    public void send() {
        final Feedback feedback = new Feedback(name.getText().toString(), phone.getText().toString(), message.getText().toString());
        new Thread(new Runnable() {
            @Override
            public void run() {
                new TaxiDataProvider(getActivity()).leaveFeedback(feedback, new Callback<Feedback>() {
                    @Override
                    public void success(Feedback feedback, Response response) {

                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {

                    }
                });
            }
        }).start();
        hideDialog();
    }



}
