package com.isolutions.taxi5api.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by George Alekhovitch on 09.01.2015.
 */
public class Route implements Serializable {
    public Integer id;
    public String name;
    public Location location;
    public String comment;
    @SerializedName("discr")
    public String description;
    public String order;
    public int index;
    public String type;
}
