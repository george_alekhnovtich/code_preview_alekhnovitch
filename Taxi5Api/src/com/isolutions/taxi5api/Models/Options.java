package com.isolutions.taxi5api.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by George Alekhovitch on 14.01.2015.
 */
public class Options implements Serializable {
    public Integer animals;
    public Integer escort;
    @SerializedName("test_orders")
    public Integer testOrders;
}
