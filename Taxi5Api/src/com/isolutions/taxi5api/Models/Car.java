package com.isolutions.taxi5api.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by George Alekhovitch on 14.01.2015.
 */
public class Car implements Serializable {

    public String model;
    @SerializedName("color_rus")
    public String colorRus;
    public Driver driver;
    @SerializedName("driver_name")
    public String driverName;
    public String color;
    public String license;

    public static final String DRIVER_NAME = "driver_name";
    public static final String LISENSE = "license";
}
